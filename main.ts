const {app, BrowserWindow} = require('electron');
const path = require('path');
const url = require('url');

let win;

const windowOptions = {
  width: 1280,
  height: 900,
  frame: false
};

function createWindow () {
  // TODO: Remember window size and position
  win = new BrowserWindow(windowOptions);

  // load the dist folder from Angular
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'dist/index.html'),
    protocol: 'file:',
    slashes: true
  }));

  // Open the DevTools optionally:
  // win.webContents.openDevTools()

  win.on('closed', () => {
    win = null;
  });
}

app.on('ready', createWindow);


app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});
