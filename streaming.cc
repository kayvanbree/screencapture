#include <string>
#include <iostream>

struct Streaming {
  static void sayHello(std::string name) {
    std::cout
      << "Hello, "
      << name << "!\n";
  }
};

#include "nbind/nbind.h"

NBIND_CLASS(Streaming) {
  method(sayHello);
}
