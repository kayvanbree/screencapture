import { Component, OnInit } from '@angular/core';

const { desktopCapturer } = window.require('electron');

@Component({
  selector: 'app-video-preview',
  templateUrl: './video-preview.component.html',
  styleUrls: ['./video-preview.component.css']
})
export class VideoPreviewComponent implements OnInit {

  private selectedScreenSource;
  private streaming: Boolean = false;
  private streamingUrl = 'rtmp://live-ams.twitch.tv/app/';
  private streamKey = 'live_187885212_MHBilyil8eylaLXFwI43JUF8e9c2IV';

  constructor() {
  }

  ngOnInit() {
  }

  handleStream(stream) {
    const video = document.querySelector('video')
    video.srcObject = stream
    video.onloadedmetadata = (e) => video.play();

    if (this.streaming) {

    }
  }

  handleError(e) {
    console.log(e);
  }

  onItemDrop(e: any) {
    console.log('Dropped item');
    console.log(e.dragData);
    this.selectedScreenSource = e.dragData;
    this.loadCurrentScreenSource();
  }

  loadCurrentScreenSource() {
    const constraints = {
      audio: false,
      video: {
        mandatory: {
          chromeMediaSource: 'desktop',
          chromeMediaSourceId: this.selectedScreenSource.id,
          minWidth: 1280,
          maxWidth: 1280,
          minHeight: 720,
          maxHeight: 720
        }
      }
    } as any;

    navigator.mediaDevices.getUserMedia(constraints)
      .then((stream) => this.handleStream(stream))
      .catch((e) => this.handleError(e));
    return;
  }
}
