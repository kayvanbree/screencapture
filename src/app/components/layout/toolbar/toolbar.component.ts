import { Component, OnInit } from '@angular/core';
const remote = window.require('electron').remote;

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  private window;

  constructor() { }

  ngOnInit() {
    this.window = remote.getCurrentWindow();
  }

  minimize() {
    this.window.minimize();
  }

  exit() {
    this.window.close();
  }

  toggleMaximize() {
    if (this.window.isMaximized()) {
      this.window.unmaximize();
    } else {
      this.window.maximize();
    }
  }
}
