import { Component, OnInit } from '@angular/core';
import {VideosourcesService} from '../../services/videosources.service';

@Component({
  selector: 'app-video-source-list',
  templateUrl: './video-source-list.component.html',
  styleUrls: ['./video-source-list.component.css']
})
export class VideoSourceListComponent implements OnInit {
  private videoSources;

  constructor(private videosourcesService: VideosourcesService) {
    this.videoSources = videosourcesService.getList();
  }

  ngOnInit() {
  }
}
