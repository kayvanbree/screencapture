import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoSourceListComponent } from './video-source-list.component';

describe('VideoSourceListComponent', () => {
  let component: VideoSourceListComponent;
  let fixture: ComponentFixture<VideoSourceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoSourceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoSourceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
