import { NgModule } from '@angular/core';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonModule, MatIconModule} from '@angular/material';

const MATERIALS = [
  BrowserAnimationsModule,
  MatToolbarModule,
  MatSidenavModule,
  MatIconModule,
  MatButtonModule
];

@NgModule({
  imports: MATERIALS,
  exports: MATERIALS
})
export class MaterialsModule { }
