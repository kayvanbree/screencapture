import { Component } from '@angular/core';
import { ElectronService } from 'ngx-electron';

import * as nbind from 'nbind';
import * as LibTypes from '../../lib-types';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  private greeting;

  constructor(private electronService: ElectronService) {

  }
}
