import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxElectronModule } from 'ngx-electron';

import { AppComponent } from './app.component';
import { VideoPreviewComponent } from './components/video-preview/video-preview.component';
import { VideoSourceListComponent } from './components/video-source-list/video-source-list.component';
import {MaterialsModule} from './Modules/materials/materials.module';
import { ToolbarComponent } from './components/layout/toolbar/toolbar.component';
import {VideosourcesService} from './services/videosources.service';
import {NgDragDropModule} from 'ng-drag-drop';
import {MatListModule, MatMenuModule} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    VideoPreviewComponent,
    VideoSourceListComponent,
    ToolbarComponent
  ],
  imports: [
    BrowserModule,
    NgxElectronModule,
    MaterialsModule,
    NgDragDropModule.forRoot(),
    MatListModule,
    MatMenuModule,
  ],
  providers: [
    VideosourcesService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
