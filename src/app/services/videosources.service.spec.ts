import { TestBed, inject } from '@angular/core/testing';

import { VideosourcesService } from './videosources.service';

describe('VideosourcesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VideosourcesService]
    });
  });

  it('should be created', inject([VideosourcesService], (service: VideosourcesService) => {
    expect(service).toBeTruthy();
  }));
});
