import { Injectable } from '@angular/core';
const { desktopCapturer } = window.require('electron');

@Injectable()
export class VideosourcesService {
  private videoSources = [];

  constructor() { }

  getList() {
    console.log('Getting video sources...');
    desktopCapturer.getSources(
      {
        types: [
          'window',
          'screen'
        ]
      },
      (error, sources) => {
        if (error) {
          throw error;
        }

        for (let i = 0; i < sources.length; ++i) {
          this.videoSources[i] = sources[i];
        }
      }
    );
    console.log(this.videoSources);
    return this.videoSources;
  }
}
